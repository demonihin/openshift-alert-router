# Openshift Alert Router

This application is developed specifically to route Alerts from Prometheus stack AlertManager
to Kubernetes Namespace owners using LDAP queries to find user emails.

Openshift by default annotates every Namespace with a creator's annotation:
`openshift.io/requester` which contains a login of the creator.

The application's message routing is below:

![Message Routing](Message_Routing.png)

Helm Chart for the application is in the [chart](chart/) directory. 
Tunable parameters are documented in [values.yaml](chart/values.yaml)

Configuration example is in [config-example.yaml](config-example.yaml) file.