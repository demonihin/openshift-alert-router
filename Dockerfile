ARG builderImage=docker.io/library/golang:1.16-alpine
ARG runnerImage=docker.io/library/alpine

FROM ${builderImage} AS builder
WORKDIR /usr/src/go
ADD . .
RUN cd cmd && go build -o /usr/src/go/alert-router

FROM ${runnerImage}
COPY --from=builder /usr/src/go/alert-router /alert-router
COPY --from=builder /usr/src/go/templates /usr/share/alert-router/templates

ENV KUBECONFIG=""
CMD ["/alert-router", "-config", "/config/config.yaml", "-templates", "/usr/share/alert-router/templates/"]
