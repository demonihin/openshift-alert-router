AlertsURL: {{ .ExternalURL }}
Status: {{ .Status }}

{{- if .GroupLabels }}
GroupLabels:
{{- range $k, $v := .GroupLabels }}
  {{ $k }}: {{ $v }}
{{- end }}
{{- end }}
{{ if .CommonLabels }}
CommonLabels:
{{- range $k, $v := .CommonLabels }}
  {{ $k }}: {{ $v }}
{{- end }}
{{- end }}
{{ if .CommonAnnotations }}
CommonAnnotations:
{{- range $k, $v := .CommonAnnotations }}
  {{ $k }}: {{ $v }}
{{- end }}
{{- end }}

Alerts:
{{- range $i, $alert := .Alerts }}
  - Status: {{ $alert.Status }}
    StartsAt: {{ $alert.StartsAt }}
    EndsAt: {{ $alert.EndsAt }}
    GeneratorURL: {{ $alert.GeneratorURL }}
    Fingerprint: {{ $alert.Fingerprint }}
    Labels:
  	{{- range $k, $v := $alert.Labels }}
      {{ $k }}: {{ $v }}
  	{{- end }}
    Annotations:
  	{{- range $k, $v := $alert.Annotations }}
      {{ $k }}: {{ $v }}
  	{{- end }}
{{- end }}