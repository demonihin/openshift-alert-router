<!--
Thanks to the https://github.com/mailgun/transactional-email-templates project
for their template!
---
The MIT License (MIT)

Copyright (c) 2014 Mailgun

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->

<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    box-sizing: border-box;
    font-size: 14px;
    margin: 0;
  ">

<head>
  <meta name="viewport" content="width=device-width" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Alerts: {{ .Status }}</title>

  <style type="text/css">
    img {
      max-width: 100%;
    }

    body {
      -webkit-font-smoothing: antialiased;
      -webkit-text-size-adjust: none;
      width: 100% !important;
      height: 100%;
      line-height: 1.6em;
      background-color: #f6f6f6;
      font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
      box-sizing: border-box;
      font-size: 14px;
      margin: 0;
    }

    .aligncenter {
      font-size: 12px;
      vertical-align: top;
      color: #999;
      text-align: center;
    }

    tr {
      margin: 0;
    }

    .body-wrap {
      width: 100%;
      background-color: #f6f6f6;
      margin: 0;
    }

    .container {
      display: block !important;
      max-width: 600px !important;
      clear: both !important;
      margin: 0 auto;
    }

    .content {
      max-width: 600px;
      display: block;
      margin: 0 auto;
      padding: 20px;
    }

    .content-wrap {
      vertical-align: top;
      margin: 0;
      padding: 20px;
    }

    .content-block {
      vertical-align: top;
      margin: 0;
      padding: 0 0 20px;
    }

    .main {
      border-radius: 3px;
      background-color: #fff;
      margin: 0;
      border: 1px solid #e9e9e9;
    }

    .alert {
      font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
      box-sizing: border-box;
      font-size: 16px;
      vertical-align: top;
      color: #fff;
      font-weight: 500;
      text-align: center;
      border-radius: 3px 3px 0 0;
      margin: 0;
      padding: 20px;
    }

    .alert-warning {
      background-color: #ff9f00;
    }

    .alert-resolved {
      background-color: #0ccf5d;
    }

    .alert-critical {
      background-color: #f51d1d;
    }

    .btn-primary {
      color: #fff;
      text-decoration: none;
      line-height: 2em;
      font-weight: bold;
      text-align: center;
      cursor: pointer;
      display: inline-block;
      border-radius: 5px;
      text-transform: capitalize;
      background-color: #348eda;
      margin: 0;
      border-color: #348eda;
      border-style: solid;
      border-width: 10px 20px;
    }

    .footer {
      width: 100%;
      clear: both;
      color: #999;
      margin: 0;
      padding: 20px;
    }

    .footer .content-block {
      text-decoration: underline;
    }

    @media only screen and (max-width: 640px) {
      body {
        padding: 0 !important;
      }

      h1 {
        font-weight: 800 !important;
        margin: 20px 0 5px !important;
      }

      h2 {
        font-weight: 800 !important;
        margin: 20px 0 5px !important;
      }

      h3 {
        font-weight: 800 !important;
        margin: 20px 0 5px !important;
      }

      h4 {
        font-weight: 800 !important;
        margin: 20px 0 5px !important;
      }

      h1 {
        font-size: 22px !important;
      }

      h2 {
        font-size: 18px !important;
      }

      h3 {
        font-size: 16px !important;
      }

      .container {
        padding: 0 !important;
        width: 100% !important;
      }

      .content {
        padding: 0 !important;
      }

      .content-wrap {
        padding: 10px !important;
      }

      .invoice {
        width: 100% !important;
      }
    }
  </style>
</head>

<body itemscope itemtype="http://schema.org/EmailMessage">
  <table class="body-wrap">
    <tr>
      <td class="container" width="600">
        <div class="content">
          <table class="main" cellpadding="0" cellspacing="0">
            <tr>
              {{- $alertsCount := len .Alerts -}}

              {{- $alertClass := "" -}}
              {{- if eq .Status "firing" -}}
              {{- $alertClass = "alert-critical" -}}
              {{- else -}}
              {{- $alertClass = "alert-resolved" -}}
              {{- end -}}

              <td class="alert {{ $alertClass }}">
                {{- if eq $alertsCount 1 }}
                1 alert is {{ .Status }}
                {{- else }}
                {{ $alertsCount }} alerts are {{ .Status }}
                {{- end }}
              </td>
            </tr>
            <tr>
              <td class="content-wrap">
                <a href="{{ .ExternalURL }}" class="btn-primary">View in
                  AlertManager</a>
              </td>
            </tr>
            <tr>
              <td class="content-wrap">
                <table width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="content-block">
                      <strong>Alerts:</strong>
                    </td>
                  </tr>
                </table>

                {{ range $alertInd, $alert := .Alerts }}
                <table width="100%" cellpadding="0" cellspacing="0"
                  class="content-block">
                  {{- if eq $alert.Status "firing" -}}
                  {{- $alertClass = "alert-critical" -}}
                  {{- else -}}
                  {{- $alertClass = "alert-resolved" -}}
                  {{- end -}}
                  <tr>
                    <td class="{{ $alertClass }}">
                      <strong>Status:</strong> {{ $alert.Status }}
                    </td>
                  </tr>
                  <tr>
                    <td><strong>Starts at:</strong> {{ $alert.StartsAt }}</td>
                  </tr>
                  {{- if eq $alert.Status "resolved" }}
                  <tr>
                    <td><strong>Ends at:</strong> {{ $alert.EndsAt }}</td>
                  </tr>
                  {{- end }}

                  <tr>
                    <td>
                      <strong>Labels:</strong>
                    </td>
                  </tr>

                  {{- range $labelKey, $labelValue := $alert.Labels }}
                  <tr>
                    <td>{{ $labelKey }}: {{ $labelValue }}</td>
                  </tr>
                  {{- end }}

                  <tr>
                    <td>
                      <strong>Annotations:</strong>
                    </td>
                  </tr>
                  {{- range $annotationKey, $annotationValue :=
                  $alert.Annotations }}
                  <tr>
                    <td>{{ $annotationKey }}: {{ $annotationValue }}</td>
                  </tr>
                  {{- end }}
                </table>
                {{- end }}
              </td>
            </tr>
          </table>
        </div>
      </td>
    </tr>
  </table>
</body>

</html>