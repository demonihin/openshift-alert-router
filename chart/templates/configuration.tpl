{{- define "whReceiver.configuration" }}
ownersLookup:
  # One of ["labels", "annotations"]
  lookupIn: {{ .Values.configuration.ownersLookup.lookupIn | quote }}
  # Key in Metadata to lookup owners in
  # values can be separated by commas, without space.
  lookupKey: {{ .Values.configuration.ownersLookup.lookupKey | quote }}
emailSender:
  insecureTLS: {{ .Values.configuration.emailSender.insecureTLS }}
  # Email From
  from: {{ .Values.configuration.emailSender.from | quote }}
  # SMTP Server and port
  smtpServer: {{ .Values.configuration.emailSender.smtpServer | quote }}
  # Authentication Login
  login: {{ .Values.configuration.emailSender.login | quote }}
  # Authentication Password
  password: {{ .Values.configuration.emailSender.password | quote }}
  # Authentication method, one of ["plain", "md5", "login", "none"]
  authMethod: {{ .Values.configuration.emailSender.authMethod | quote }}
  # Email template to use
  templateName: {{ .Values.configuration.emailSender.templateName | quote }}
  # Send Email as HTML
  isTemplateHTML: {{ .Values.configuration.emailSender.isTemplateHTML }}
  # Default owners to which alerts are sent if no owners can be found
  # If not set, alerts without owners are skipped.
  defaultRecipients: 
    {{- .Values.configuration.emailSender.defaultRecipients | toYaml | nindent 4 }}
ldapEmailLookup:
  insecureTLS: {{ .Values.configuration.ldapEmailLookup.insecureTLS }}
  ldapURL: {{ .Values.configuration.ldapEmailLookup.ldapURL | quote }}
  baseDN: {{ .Values.configuration.ldapEmailLookup.baseDN | quote }}
  bindDN: {{ .Values.configuration.ldapEmailLookup.bindDN | quote }}
  bindPassword: {{ .Values.configuration.ldapEmailLookup.bindPassword | quote }}
  querySchema: {{ .Values.configuration.ldapEmailLookup.querySchema | quote }}
{{- if and .Values.configuration.includeNamespaces .Values.configuration.excludeNamespaces }}
  {{- fail "Both .configuration.includeNamespaces and .configuration.excludeNamespaces are set. It is not supported." }}
{{- end }}
includeNamespaces:
{{- .Values.configuration.includeNamespaces | toYaml | nindent 2 }}
excludeNamespaces:
{{- .Values.configuration.excludeNamespaces | toYaml | nindent 2 }}
{{- end }}