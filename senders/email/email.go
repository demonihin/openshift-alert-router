package email

import (
	"bytes"
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"

	mail "github.com/xhit/go-simple-mail/v2"

	amtemplate "github.com/prometheus/alertmanager/template"
)

var (
	ErrInvalidArgument = errors.New("given argument is not valid")
)

type SMTPAuthMethod string

type EmailTemplateExecutor interface {
	Execute(wr io.Writer, data interface{}) error
}

const (
	SMTPAuthPlain   SMTPAuthMethod = "plain"
	SMTPAuthCRAMMD5 SMTPAuthMethod = "md5"
	SMTPAuthLogin   SMTPAuthMethod = "login"
	SMTPAuthNone    SMTPAuthMethod = "none"
)

type Sender struct {
	isHTML             bool
	insecureSkipVerify bool

	isAuthPlain bool
	isAuthMD5   bool
	isAuthLogin bool
	isAuthNone  bool

	smtpPort int

	from string

	smtpServer   string
	smtpLogin    string
	smtpPassword string

	template EmailTemplateExecutor
}

func New(emailFrom, server string, template EmailTemplateExecutor, isHTML bool) (*Sender, error) {
	if emailFrom == "" {
		return nil, fmt.Errorf("%w: emailFrom must not be empty", ErrInvalidArgument)
	}

	if server == "" {
		return nil, fmt.Errorf("%w: server must not be empty", ErrInvalidArgument)
	}

	if template == nil {
		return nil, fmt.Errorf("%w: template must not be nil", ErrInvalidArgument)
	}

	sp := strings.Split(server, ":")
	// sp string must contain 2 parts <hostname>:<port>
	if len(sp) != 2 { //nolint:gomnd
		return nil, fmt.Errorf(
			"%w: server must be in form <hostname|IP address>:<TCP port>, provided value %q",
			ErrInvalidArgument, server)
	}

	hostname := sp[0]

	// TCP port is a 16 bit unsigned int value.
	port, err := strconv.ParseUint(sp[1], 10, 16) //nolint:gomnd
	if err != nil {
		return nil, fmt.Errorf(
			"%w: %s: provided TCP port %q is not valid",
			ErrInvalidArgument, err, sp[1])
	}

	return &Sender{
		from:       emailFrom,
		smtpServer: hostname,
		smtpPort:   int(port),
		isAuthNone: true,
		template:   template,
		isHTML:     isHTML,
	}, nil
}

func NewWithAuth(
	emailFrom, server, login, password string,
	method SMTPAuthMethod,
	template EmailTemplateExecutor,
	isHTML bool) (*Sender, error) {
	snd, err := New(emailFrom, server, template, isHTML)
	if err != nil {
		return nil, err
	}

	if login == "" && method != SMTPAuthNone {
		return nil, fmt.Errorf("%w: login must not be empty", ErrInvalidArgument)
	}
	snd.smtpLogin = login

	if password == "" && method != SMTPAuthNone {
		return nil, fmt.Errorf("%w: password must not be empty", ErrInvalidArgument)
	}
	snd.smtpPassword = password

	switch method {
	case SMTPAuthPlain:
		snd.isAuthPlain = true
	case SMTPAuthCRAMMD5:
		snd.isAuthMD5 = true
	case SMTPAuthLogin:
		snd.isAuthLogin = true
	case SMTPAuthNone:
		snd.isAuthNone = true
	default:
		return nil, fmt.Errorf(
			"%w: %q is not supported authentication method",
			ErrInvalidArgument, method)
	}

	return snd, nil
}

func (s *Sender) SetInsecureSkipVerify(isInsecure bool) {
	s.insecureSkipVerify = isInsecure
}

func (s *Sender) Send(msg *amtemplate.Data, sendTo ...string) error {
	mailMsg, err := makeMessage(s.isHTML, s.template, msg, s.from, sendTo...)
	if err != nil {
		return err
	}

	conn, err := s.connectSMTP()
	if err != nil {
		return err
	}

	defer func() {
		ierr := conn.Close()
		if ierr != nil {
			panic(fmt.Errorf("%w: can not close SMTP connection", ierr))
		}
	}()

	if err := mailMsg.Send(conn); err != nil {
		return fmt.Errorf("message send error: %w", err)
	}

	return nil
}

func (s *Sender) connectSMTP() (*mail.SMTPClient, error) {
	cl := mail.NewSMTPClient()

	// AuthNone case retains nil value for SMTP client authentication method.
	if s.isAuthLogin {
		cl.Authentication = mail.AuthLogin
	} else if s.isAuthMD5 {
		cl.Authentication = mail.AuthCRAMMD5
	} else if s.isAuthPlain {
		cl.Authentication = mail.AuthPlain
	}

	if s.insecureSkipVerify {
		cl.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	}

	cl.Host = s.smtpServer
	cl.Port = s.smtpPort
	cl.ConnectTimeout = time.Second * 30

	cl.Username = s.smtpLogin
	cl.Password = s.smtpPassword

	cl.KeepAlive = true

	cl.SendTimeout = time.Second * 30

	conn, err := cl.Connect()
	if err != nil {
		return nil, fmt.Errorf("SMTP connection to %q:%d: %w", s.smtpServer, s.smtpPort, err)
	}

	return conn, nil
}

func makeMessage(
	isHTML bool,
	tmpl EmailTemplateExecutor,
	data *amtemplate.Data,
	from string, to ...string) (*mail.Email, error) {
	msg := mail.NewMSG()

	msg = msg.SetFrom(from)
	msg = msg.AddTo(to...)

	msg = msg.SetSubject(fmt.Sprintf("%s: cluster: %q, namespace: %q, pod: %q",
		data.Status,
		data.CommonLabels["cluster"],
		data.CommonLabels["namespace"],
		data.CommonLabels["pod"]))

	var bb = &bytes.Buffer{}
	if err := tmpl.Execute(bb, data); err != nil {
		return nil, fmt.Errorf("can not render email body: %w", err)
	}

	if isHTML {
		msg = msg.SetBodyData(mail.TextHTML, bb.Bytes())
	} else {
		msg = msg.SetBodyData(mail.TextPlain, bb.Bytes())
	}

	return msg, nil
}
