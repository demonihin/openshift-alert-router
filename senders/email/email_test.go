package email

import (
	"html/template"
	"testing"
	"time"

	amtemplate "github.com/prometheus/alertmanager/template"
)

func TestEmailSendOk(t *testing.T) {
	tmpl, err := template.New("test").Parse(
		`AlertsURL: {{ .ExternalURL }}
Status: {{ .Status }}

{{- if .GroupLabels }}
GroupLabels:
{{- range $k, $v := .GroupLabels }}
  {{ $k }}: {{ $v }}
{{- end }}
{{- end }}
{{ if .CommonLabels }}
CommonLabels:
{{- range $k, $v := .CommonLabels }}
  {{ $k }}: {{ $v }}
{{- end }}
{{- end }}
{{ if .CommonAnnotations }}
CommonAnnotations:
{{- range $k, $v := .CommonAnnotations }}
  {{ $k }}: {{ $v }}
{{- end }}
{{- end }}

Alerts:
{{- range $i, $alert := .Alerts }}
  - Status: {{ $alert.Status }}
    StartsAt: {{ $alert.StartsAt }}
    EndsAt: {{ $alert.EndsAt }}
    GeneratorURL: {{ $alert.GeneratorURL }}
    Fingerprint: {{ $alert.Fingerprint }}
    Labels:
  	{{- range $k, $v := $alert.Labels }}
      {{ $k }}: {{ $v }}
  	{{- end }}
    Annotations:
  	{{- range $k, $v := $alert.Annotations }}
      {{ $k }}: {{ $v }}
  	{{- end }}
{{- end }}
`,
	)
	if err != nil {
		t.Fatalf("Can not parse test template: %s", err)
	}

	var data = amtemplate.Data{
		Receiver: "My Receiver",
		Status:   "Resolved",
		Alerts: amtemplate.Alerts{
			{
				Status:       "Resolved",
				StartsAt:     time.Now(),
				EndsAt:       time.Now().Add(time.Hour),
				Fingerprint:  "fingerprint-1",
				GeneratorURL: "https://example.com/generator-url-1",
				Labels: amtemplate.KV{
					"alert-1-label-1": "alert-1-label-value-1",
					"alert-1-label-2": "alert-1-label-value-2",
				},
				Annotations: amtemplate.KV{
					"alert-1-annotation-1": "alert-1-annotation-value-1",
					"alert-1-annotation-2": "alert-1-annotation-value-2",
				},
			},
			{
				Status:       "Firing",
				StartsAt:     time.Now(),
				EndsAt:       time.Now().Add(time.Hour),
				Fingerprint:  "fingerprint-2",
				GeneratorURL: "https://example.com/generator-url-2",
				Labels: amtemplate.KV{
					"alert-2-label-1": "alert-2-label-value-1",
					"alert-2-label-2": "alert-2-label-value-2",
				},
				Annotations: amtemplate.KV{
					"alert-2-annotation-1": "alert-2-annotation-value-1",
					"alert-2-annotation-2": "alert-2-annotation-value-2",
				},
			},
		},
		GroupLabels: amtemplate.KV{
			"group-label-1": "group-label-value-1",
			"group-label-2": "group-label-value-2",
		},
		CommonAnnotations: amtemplate.KV{
			"common-annotation-1": "common-annotation-value-1",
			"common-annotation-2": "common-annotation-value-2",
		},
		CommonLabels: amtemplate.KV{
			"common-label-1": "common-label-value-1",
			"common-label-2": "common-label-value-2",
		},
		ExternalURL: "https://example.com/alerts/",
	}

	snd, err := New("test-from@example.com", "localhost:1025", tmpl, false)
	if err != nil {
		t.Fatalf("can not init email sender: %s", err)
	}

	if err := snd.Send(
		&data,
		[]string{"recepient-1@example.com", "recepient-2@example.com"}...); err != nil {
		t.Fatalf("can not send email: %s", err)
	}

}
