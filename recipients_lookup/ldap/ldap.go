package ldap

import (
	"crypto/tls"
	"errors"
	"fmt"

	"github.com/go-ldap/ldap/v3"
	"go.uber.org/zap"
)

var ErrEmptyResponse = errors.New("empty email list in response")

const LDAPPageSize = 100

type QuerySchema interface {
	RenderLDAPQuery(usernames ...string) (string, error)
	GetLookupAttributeName() string
}

type EmailsLookupper struct {
	insecureTLS bool

	ldapURL string

	bindDN       string
	bindPassword string

	baseDN        string
	queryTemplate QuerySchema
}

// New connects to the given ldap URL.
// The following schemas are supported: ldap://, ldaps://, ldapi://.
func New(ldapURL, baseDN, bindDN, bindPassword string,
	tmpl QuerySchema, insecureTLS bool) (*EmailsLookupper, error) {
	conn, err := connectLDAP(ldapURL, bindDN, bindPassword, insecureTLS)
	if err != nil {
		return nil, fmt.Errorf("can not connect to LDAP server: %w", err)
	}
	defer conn.Close()

	zap.L().Info("Connected to LDAP server",
		zap.String("URL", ldapURL),
		zap.String("BaseDN", baseDN),
	)

	return &EmailsLookupper{
		insecureTLS:   insecureTLS,
		ldapURL:       ldapURL,
		baseDN:        baseDN,
		queryTemplate: tmpl,
		bindPassword:  bindPassword,
		bindDN:        bindDN,
	}, nil
}

func (c *EmailsLookupper) LookupUsersEmails(usernames ...string) ([]string, error) {
	filter, err := c.queryTemplate.RenderLDAPQuery(usernames...)
	if err != nil {
		return nil, fmt.Errorf("can not build LDAP query: %w", err)
	}

	emailAttribute := c.queryTemplate.GetLookupAttributeName()

	conn, err := connectLDAP(c.ldapURL, c.bindDN, c.bindPassword, c.insecureTLS)
	if err != nil {
		return nil, fmt.Errorf("can not connect to LDAP server: %w", err)
	}
	defer conn.Close()

	request := ldap.NewSearchRequest(
		c.baseDN,
		ldap.ScopeWholeSubtree,
		ldap.DerefAlways,
		0, 0, false,
		filter,
		[]string{emailAttribute},
		nil)

	response, err := conn.SearchWithPaging(request, LDAPPageSize)
	if err != nil {
		return nil, fmt.Errorf("can not query LDAP: %w", err)
	}

	if len(response.Entries) == 0 {
		return nil, fmt.Errorf("%w: no email for users %+v", ErrEmptyResponse, usernames)
	}

	var result = make([]string, 0, len(response.Entries))

	for _, lent := range response.Entries {
		result = append(result, lent.GetAttributeValues(emailAttribute)...)
	}

	return result, nil
}

func connectLDAP(ldapURL, bindDN, bindPassword string, insecureTLS bool) (*ldap.Conn, error) {
	conn, err := ldap.DialURL(ldapURL,
		ldap.DialWithTLSConfig(&tls.Config{InsecureSkipVerify: insecureTLS}))
	if err != nil {
		return nil, fmt.Errorf("can not connect to LDAP server: %w", err)
	}

	if bindDN != "" && bindPassword != "" {
		if err = conn.Bind(bindDN, bindPassword); err != nil {
			return nil, fmt.Errorf("can not LDAP Bind: %w", err)
		}
	}

	return conn, nil
}
