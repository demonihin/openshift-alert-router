package ldap

import (
	"errors"
	"strings"
)

var ErrEmptyRequest = errors.New("empty usernames list given")

type SchemaActiveDirectory struct{}

func (s SchemaActiveDirectory) RenderLDAPQuery(usernames ...string) (string, error) {
	if len(usernames) == 0 {
		return "", ErrEmptyRequest
	}

	var result = &strings.Builder{}

	result.WriteString("(|")

	for _, un := range usernames {
		result.WriteString("(sAMAccountName=")
		result.WriteString(un)
		result.WriteString(")")
	}

	result.WriteString(")")

	return result.String(), nil
}

func (s SchemaActiveDirectory) GetLookupAttributeName() string {
	return "mail"
}
