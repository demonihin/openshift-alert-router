package main

import (
	"flag"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type Flags struct {
	LogLevel *zapcore.Level

	ReceiverConfigPath string
	BindSocket         string
	TemplateLookupPath string
}

func (f *Flags) InitFlags() {
	flag.StringVar(&f.ReceiverConfigPath, "config", "config.yaml", "path to configuration file")
	flag.StringVar(&f.BindSocket, "bind", ":8080", "defines socket to listen HTTP on")
	flag.StringVar(&f.TemplateLookupPath, "templates", "templates/", "path to lookup templates in")

	f.LogLevel = zap.LevelFlag("logLevel", zap.InfoLevel, "log level, one of [PANIC, ERROR, INFO, DEBUG]")

	flag.Parse()
}
