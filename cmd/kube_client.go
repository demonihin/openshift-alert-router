package main

import (
	"flag"
	"fmt"
	"os"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

func getKubeconfigPath() (string, bool) {
	// Try usual KUBECONFIG.
	kubeconfig := os.Getenv("KUBECONFIG")

	var tmpKConfig string

	flag.StringVar(&tmpKConfig, "kubeconfig", "", "absolute path to the kubeconfig file")

	flag.Parse()

	if tmpKConfig != "" {
		return tmpKConfig, true
	}

	if kubeconfig != "" {
		return kubeconfig, true
	}

	return "", false
}

func initK8SClient() (*kubernetes.Clientset, error) {
	var config *rest.Config

	// Try explicit configuration.
	path, found := getKubeconfigPath()
	if found {
		cf, err := clientcmd.BuildConfigFromFlags("", path)
		if err != nil {
			return nil, fmt.Errorf(
				"can not build Kubernetes config from KUBECONFIG=%q: %w",
				path, err)
		}

		config = cf
	} else {
		// Try in cluster config.
		cf, err := rest.InClusterConfig()
		if err != nil {
			return nil, fmt.Errorf("can not load InClusterConfig: %w", err)
		}

		config = cf
	}

	// create the clientset.
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, fmt.Errorf("can not create Kubernetes client: %w", err)
	}

	return clientset, nil
}
