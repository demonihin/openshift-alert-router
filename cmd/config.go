package main

import (
	"fmt"
	"os"

	"gitlab.com/demonihin/openshift-alert-router/ownersk8s"
	"gitlab.com/demonihin/openshift-alert-router/senders/email"
	"k8s.io/apimachinery/pkg/util/yaml"
)

// ConfigFileBufferSize size too lookup to detect whether the configuration is JSON or YAML.
const ConfigFileBufferSize = 1024

type NotificationOwnersLookupConfig struct {
	LookupIn  ownersk8s.LookupIn `json:"lookupIn,omitempty" yaml:"lookupIn,omitempty"`
	LookupKey string             `json:"lookupKey,omitempty" yaml:"lookupKey,omitempty"`
}

type SenderEmailConfig struct {
	InsecureTLS bool `json:"insecureTLS,omitempty" yaml:"insecureTLS,omitempty"`

	IsTemplateHTML bool   `json:"isTemplateHTML,omitempty" yaml:"isTemplateHTML,omitempty"`
	TemplateName   string `json:"templateName,omitempty" yaml:"templateName,omitempty"`

	From       string               `json:"from,omitempty" yaml:"from,omitempty"`
	SMTPServer string               `json:"smtpServer,omitempty" yaml:"smtpServer,omitempty"`
	Login      string               `json:"login,omitempty" yaml:"login,omitempty"`
	Password   string               `json:"password,omitempty" yaml:"password,omitempty"`
	AuthMethod email.SMTPAuthMethod `json:"authMethod,omitempty" yaml:"authMethod,omitempty"`

	DefaultRecipients []string `json:"defaultRecipients,omitempty" yaml:"defaultRecipients,omitempty"`
}

type LDAPEmailLookupConfig struct {
	InsecureTLS  bool   `json:"insecureTLS,omitempty" yaml:"insecureTLS,omitempty"`
	LDAPURL      string `json:"ldapURL,omitempty" yaml:"ldapURL,omitempty"`
	BindDN       string `json:"bindDN,omitempty" yaml:"bindDN,omitempty"`
	BindPassword string `json:"bindPassword,omitempty" yaml:"bindPassword,omitempty"`
	BaseDN       string `json:"baseDN,omitempty" yaml:"baseDN,omitempty"`
	QuerySchema  string `json:"querySchema,omitempty" yaml:"querySchema,omitempty"`
}

type Config struct {
	IncludeNamespaces []string                       `json:"includeNamespaces,omitempty" yaml:"includeNamespaces,omitempty"` //nolint:lll
	ExcludeNamespaces []string                       `json:"excludeNamespaces,omitempty" yaml:"excludeNamespaces,omitempty"` //nolint:lll
	OwnersLookup      NotificationOwnersLookupConfig `json:"ownersLookup,omitempty" yaml:"ownersLookup,omitempty"`           //nolint:lll
	EmailSender       SenderEmailConfig              `json:"emailSender,omitempty" yaml:"emailSender,omitempty"`             //nolint:lll
	LDAPEmailLookup   LDAPEmailLookupConfig          `json:"ldapEmailLookup,omitempty" yaml:"ldapEmailLookup,omitempty"`     //nolint:lll
}

func (c *Config) Parse(filePath string) error {
	file, err := os.Open(filePath)
	if err != nil {
		return fmt.Errorf("can not open configuration: %w", err)
	}
	defer file.Close()

	if err := yaml.NewYAMLOrJSONDecoder(file, ConfigFileBufferSize).Decode(c); err != nil {
		return err
	}

	// Set defaults.
	if c.EmailSender.TemplateName == "" {
		c.EmailSender.TemplateName = "plaintext.tpl"
	}

	return nil
}
