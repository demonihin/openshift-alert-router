package main

import (
	"errors"

	amtemplate "github.com/prometheus/alertmanager/template"
)

const AlertManagerNamespaceLabelKey = "namespace"

var (
	ErrEmptyNamespace  = errors.New("empty Namespace name given")
	ErrMultipleFilters = errors.New("excluded and Included namespaces are defined. Only one of them must be set") //nolint:lll
)

type NamespaceSkipper struct {
	includeNamespaces []string
	excludeNamespaces []string
}

func NewNamespaceSkipper(
	includeNamespaces []string,
	excludeNamespaces []string) (*NamespaceSkipper, error) {
	if len(excludeNamespaces) != 0 && len(includeNamespaces) != 0 {
		return nil, ErrMultipleFilters
	}

	return &NamespaceSkipper{
		includeNamespaces: includeNamespaces,
		excludeNamespaces: excludeNamespaces,
	}, nil
}

func (sk *NamespaceSkipper) Skip(notification *amtemplate.Data) (bool, error) {
	namespace := notification.CommonLabels[AlertManagerNamespaceLabelKey]

	if namespace == "" {
		return false, nil
	}

	if len(sk.includeNamespaces) > 0 {
		return whiteList(sk.includeNamespaces, namespace)
	}

	if len(sk.excludeNamespaces) > 0 {
		return blackList(sk.excludeNamespaces, namespace)
	}

	return false, nil
}

// whiteList - only included namespaces are NOT Skipped.
func whiteList(includedNamespaces []string, namespace string) (bool, error) {
	for _, v := range includedNamespaces {
		if v == namespace {
			return false, nil
		}
	}

	return true, nil
}

// blackList - only excluded namespaces are Skipped.
func blackList(excludedNamespaces []string, namespace string) (bool, error) {
	for _, v := range excludedNamespaces {
		if v == namespace {
			return true, nil
		}
	}

	return false, nil
}
