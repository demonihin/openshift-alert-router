package main

import (
	"fmt"
	"html/template"
	"net/http"
	"os"
	"path"

	"gitlab.com/demonihin/openshift-alert-router/ownersk8s"
	"gitlab.com/demonihin/openshift-alert-router/recipients_lookup/ldap"
	"gitlab.com/demonihin/openshift-alert-router/senders/email"
	"gitlab.com/demonihin/openshift-alert-router/wh"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func initEmailSender(cfg *SenderEmailConfig, templatesLookupPath string) (*email.Sender, error) {
	tmpl, err := template.ParseFiles(path.Join(templatesLookupPath, cfg.TemplateName))
	if err != nil {
		return nil, fmt.Errorf("can not parse message template: %w", err)
	}

	snd, err := email.NewWithAuth(
		cfg.From, cfg.SMTPServer,
		cfg.Login, cfg.Password,
		cfg.AuthMethod,
		tmpl,
		cfg.IsTemplateHTML)
	if err != nil {
		return nil, err
	}

	if cfg.InsecureTLS {
		snd.SetInsecureSkipVerify(true)
	}

	return snd, nil
}

func initLDAPEmailLookupper(ldapEmailLookup *LDAPEmailLookupConfig) (*ldap.EmailsLookupper, error) {
	return ldap.New(ldapEmailLookup.LDAPURL,
		ldapEmailLookup.BaseDN,
		ldapEmailLookup.BindDN,
		ldapEmailLookup.BindPassword,
		ldap.SchemaActiveDirectory{},
		ldapEmailLookup.InsecureTLS)
}

func main() {
	// Flags parser.
	var flags = &Flags{}

	flags.InitFlags()

	logLevel := zap.NewAtomicLevel()
	logLevel.SetLevel(*flags.LogLevel)

	prodLog := zap.New(
		zapcore.NewCore(zapcore.NewJSONEncoder(zap.NewProductionEncoderConfig()),
			zapcore.AddSync(os.Stdout),
			logLevel))

	zap.ReplaceGlobals(prodLog)

	zap.L().Info("Starting application")

	zap.L().Info("Loading application configuration")
	var receiverConfig = &Config{}

	if err := receiverConfig.Parse(flags.ReceiverConfigPath); err != nil {
		zap.L().Fatal("can not load application configuration", zap.Error(err))
	}
	zap.L().Info("Loaded application configuration")

	zap.L().Info("Configuring Namespace based filter")
	skipper, err := NewNamespaceSkipper(receiverConfig.IncludeNamespaces, receiverConfig.ExcludeNamespaces)
	if err != nil {
		zap.L().Fatal("can not configure Namespace based filter", zap.Error(err))
	}
	zap.L().Info("Configured Namespace based filter")

	zap.L().Info("Initializing Kubernetes client")
	k8sClient, err := initK8SClient()
	if err != nil {
		zap.L().Fatal("can not initialize Kubernetes API client", zap.Error(err))
	}
	zap.L().Info("Kubernetes client initialized")

	zap.L().Info("Configuring Notification owners getter")
	ownerGetter, err := ownersk8s.New(k8sClient,
		receiverConfig.OwnersLookup.LookupIn,
		receiverConfig.OwnersLookup.LookupKey)
	if err != nil {
		zap.L().Fatal("can not configure Notification owners getter", zap.Error(err))
	}
	zap.L().Info(
		"Notifications owner lookup is configured",
		zap.Reflect("LookupIn", receiverConfig.OwnersLookup.LookupIn),
		zap.Reflect("LookupKey", receiverConfig.OwnersLookup.LookupKey),
	)

	zap.L().Info("Configuring Email sender")
	emailSender, err := initEmailSender(&receiverConfig.EmailSender, flags.TemplateLookupPath)
	if err != nil {
		zap.L().Fatal(
			"can not configure Email sender",
			zap.Error(err))
	}
	zap.L().Info(
		"Configured Email sender",
		zap.String("From", receiverConfig.EmailSender.From),
		zap.String("SMTP Server", receiverConfig.EmailSender.SMTPServer),
	)

	zap.L().Info("Configuring LDAP Email address lookup")
	emailLkp, err := initLDAPEmailLookupper(&receiverConfig.LDAPEmailLookup)
	if err != nil {
		zap.L().Fatal("can not configure LDAP Email address lookup",
			zap.Error(err))
	}
	zap.L().Info(
		"LDAP Email address lookup is configured",
		zap.String("LDAP URL", receiverConfig.LDAPEmailLookup.LDAPURL),
		zap.String("LDAP Schema", receiverConfig.LDAPEmailLookup.QuerySchema),
	)

	zap.L().Info("Starting HTTP server", zap.String("bind", flags.BindSocket))
	handler := wh.New(ownerGetter, emailLkp, receiverConfig.EmailSender.DefaultRecipients,
		emailSender, os.Stdout, skipper)

	zap.L().Fatal(
		"Stopped HTTP server",
		zap.Error(http.ListenAndServe(flags.BindSocket, handler)),
	)
}
