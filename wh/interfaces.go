package wh

import (
	"context"

	amtemplate "github.com/prometheus/alertmanager/template"
)

type NotificationOwnersGetter interface {
	GetNotificationOwnersCtx(ctx context.Context, data *amtemplate.Data) ([]string, error)
}

type EmailLookupper interface {
	LookupUsersEmails(usernames ...string) ([]string, error)
}

type EmailSender interface {
	Send(msg *amtemplate.Data, sendTo ...string) error
}

type NamespaceSkipper interface {
	Skip(notification *amtemplate.Data) (bool, error)
}
