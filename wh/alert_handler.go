package wh

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	amtemplate "github.com/prometheus/alertmanager/template"
	"go.uber.org/zap"

	"k8s.io/apimachinery/pkg/util/uuid"
)

func getEmailsCtx(
	ctx context.Context,
	ownersGetter NotificationOwnersGetter,
	emailLookupper EmailLookupper,
	defaultEmails []string,
	logger *zap.Logger,
	notification *amtemplate.Data) (emails []string, err error) {
	// Catch error and set default if possible.
	defer func() {
		if err != nil && len(defaultEmails) != 0 {
			emails = defaultEmails
			err = nil

			logger.Debug("Use default recipients list", zap.Strings("To", emails))
		}
	}()

	notifOwners, err := ownersGetter.GetNotificationOwnersCtx(ctx, notification)
	if err != nil {
		return nil, fmt.Errorf("can not get onwers: %w", err)
	}

	logger.Debug("Notification owners", zap.Strings("owners", notifOwners))

	emails, err = emailLookupper.LookupUsersEmails(notifOwners...)
	if err != nil {
		return nil, fmt.Errorf("can not lookup email: %w", err)
	}

	logger.Debug("Got recipients", zap.Strings("To", emails))

	return emails, nil
}

func createNotificationHandler(
	ownersGetter NotificationOwnersGetter,
	emailLookupper EmailLookupper,
	defaultEmailRecipients []string,
	emailSender EmailSender,
	filter NamespaceSkipper) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()

		logger := zap.L().With(zap.String("uuid", string(uuid.NewUUID())))
		logger.Debug("Received WebHook", zap.String("client", r.RemoteAddr))

		var notification amtemplate.Data

		if err := json.NewDecoder(r.Body).Decode(&notification); err != nil {
			writeError(fmt.Errorf("can not decode JSON request body: %w", err),
				http.StatusBadRequest, rw, logger)
			return
		}

		logger.Debug("Request data", zap.Reflect("data", &notification))

		skip, err := filter.Skip(&notification)
		if err != nil {
			writeError(fmt.Errorf("can not check whether skip namespace or not: %w", err),
				http.StatusInternalServerError, rw, logger)
			return
		}

		logger.Debug("Is request skipped", zap.Bool("skip", skip))

		if skip {
			rw.WriteHeader(http.StatusOK)
			_, _ = fmt.Fprintf(rw, "Notification is filtered out")

			return
		}

		emails, err := getEmailsCtx(
			r.Context(),
			ownersGetter, emailLookupper, defaultEmailRecipients,
			logger, &notification)
		if err != nil {
			writeError(fmt.Errorf("can not get recipients: %w", err),
				http.StatusInternalServerError, rw, logger)
			return
		}

		if err := emailSender.Send(&notification, emails...); err != nil {
			writeError(
				fmt.Errorf("can not send email: %w", err),
				http.StatusInternalServerError, rw, logger)
			return
		}

		rw.WriteHeader(http.StatusOK)

		if r.URL.Query().Get("debug") == "true" {
			_, _ = fmt.Fprintf(rw, "Emails are sent to %+v", emails)
		}
	}
}
