package wh

import "net/http"

func createHealthCheckerStub() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		rw.Write([]byte("Ok"))
	}
}
