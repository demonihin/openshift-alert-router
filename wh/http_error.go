package wh

import (
	"fmt"
	"net/http"

	"go.uber.org/zap"
)

func writeError(err error, code int, w http.ResponseWriter, logger *zap.Logger) {
	logger.Error("Can not handle request", zap.Error(err))

	w.WriteHeader(code)
	_, _ = fmt.Fprintf(w, "Can not handle request: %s", err)
}
