package wh

import (
	"io"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

type Handler struct {
	logStream io.Writer
	router    *mux.Router

	ownersGetter   NotificationOwnersGetter
	emailLookupper EmailLookupper
	emailSender    EmailSender
	filter         NamespaceSkipper

	defaultEmailRecipients []string
}

func New(
	ownersGetter NotificationOwnersGetter,
	emailLookupper EmailLookupper,
	defaultEmailRecipients []string,
	emailSender EmailSender,
	logStream io.Writer,
	filter NamespaceSkipper) *Handler {
	if logStream == nil {
		logStream = os.Stdout
	}

	var srv = &Handler{
		logStream:              logStream,
		router:                 mux.NewRouter(),
		ownersGetter:           ownersGetter,
		emailLookupper:         emailLookupper,
		emailSender:            emailSender,
		filter:                 filter,
		defaultEmailRecipients: defaultEmailRecipients,
	}

	srv.initRouter()

	return srv
}

func (s *Handler) initRouter() {
	s.router.Handle("/send-alert",
		handlers.ContentTypeHandler(
			createNotificationHandler(
				s.ownersGetter, s.emailLookupper, s.defaultEmailRecipients,
				s.emailSender, s.filter),
			"application/json"))

	s.router.Handle("/status", createHealthCheckerStub())
}

func (s *Handler) ServeHTTP(wr http.ResponseWriter, req *http.Request) {
	handlers.CombinedLoggingHandler(s.logStream, s.router).ServeHTTP(wr, req)
}
