package ownersk8s

import (
	"context"
	"errors"
	"fmt"
	"strings"

	amtemplate "github.com/prometheus/alertmanager/template"
	kerrors "k8s.io/apimachinery/pkg/api/errors"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

var (
	ErrInvalidArgument  = errors.New("invalid argument")
	ErrNoNamespaceLabel = errors.New("namespace name is not in alert's common labels")
	ErrNoPodLabel       = errors.New("pod name is not in alert's common labels")
	ErrNoOwnerData      = errors.New("can not detect owner")
)

type LookupIn string

const (
	LookupInLabels      LookupIn = "labels"
	LookupInAnnotations LookupIn = "annotations"
)

type podOwnersLookupper func(ctx context.Context, k8s kubernetes.Interface,
	namespace, pod, key string) ([]string, error)
type namespaceOwnersLookupper func(ctx context.Context, k8s kubernetes.Interface,
	namespace, key string) ([]string, error)

type OwnersGetter struct {
	podLKP       podOwnersLookupper
	namespaceLKP namespaceOwnersLookupper

	kclient kubernetes.Interface

	lookupKey string
}

func New(cl kubernetes.Interface, lookupIn LookupIn, lookupKey string) (*OwnersGetter, error) {
	if cl == nil {
		return nil, fmt.Errorf("%w: kubernetes.Interface must not be nil", ErrInvalidArgument)
	}

	if lookupKey == "" {
		return nil, fmt.Errorf("%w: lookupKey must not be empty", ErrInvalidArgument)
	}

	var ogt = OwnersGetter{
		kclient:   cl,
		lookupKey: lookupKey,
	}

	switch lookupIn {
	case LookupInLabels:
		ogt.namespaceLKP = getNamespaceLabelContext
		ogt.podLKP = getPodLabelContext
	case LookupInAnnotations:
		ogt.namespaceLKP = getNamespaceAnnotationContext
		ogt.podLKP = getPodAnnotationContext
	default:
		return nil, fmt.Errorf("%w: %q is not a valid value for lookupIn", ErrInvalidArgument, lookupIn)
	}

	return &ogt, nil
}

func (own *OwnersGetter) GetNotificationOwnersCtx(
	ctx context.Context, data *amtemplate.Data) ([]string, error) {
	namespace := data.CommonLabels["namespace"]
	if namespace == "" {
		return nil, ErrNoNamespaceLabel
	}

	podName := data.CommonLabels["pod"]
	if podName == "" {
		// Lookup Namespace only.
		return own.namespaceLKP(ctx, own.kclient, namespace, own.lookupKey)
	}

	// Lookup Pod.
	podOwners, err := own.podLKP(ctx, own.kclient, namespace, podName, own.lookupKey)
	if err != nil {
		// Threat Pod Not Found and No data for Pod
		// as no errors.
		if !kerrors.IsNotFound(err) && !errors.Is(err, ErrNoOwnerData) {
			return nil, err
		}
	} else {
		return podOwners, nil
	}

	// Lookup Namespace.
	namespaceOwners, err := own.namespaceLKP(ctx, own.kclient, namespace, own.lookupKey)
	if err != nil {
		return nil, err
	}

	return namespaceOwners, nil
}

func getPodLabelContext(ctx context.Context, k8s kubernetes.Interface,
	namespace, pod, labelKey string) ([]string, error) {
	// Lookup Pod.
	v1pod, err := k8s.CoreV1().Pods(namespace).Get(ctx, pod, v1.GetOptions{})
	if err != nil {
		return nil, fmt.Errorf("can not get a Pod %s/%s: %w",
			namespace, pod, err)
	}

	labelVal, ok := v1pod.Labels[labelKey]
	if !ok {
		return nil, fmt.Errorf("%w: no Pod label %q value", ErrNoOwnerData, labelKey)
	}

	return strings.Split(labelVal, ","), nil
}

func getPodAnnotationContext(ctx context.Context, k8s kubernetes.Interface,
	namespace, pod, annotationKey string) ([]string, error) {
	// Lookup Pod.
	v1pod, err := k8s.CoreV1().Pods(namespace).Get(ctx, pod, v1.GetOptions{})
	if err != nil {
		return nil, fmt.Errorf("can not get a Pod %s/%s: %w",
			namespace, pod, err)
	}

	annotVal, ok := v1pod.Annotations[annotationKey]
	if !ok {
		return nil, fmt.Errorf("%w: no Pod annotation %q value", ErrNoOwnerData, annotationKey)
	}

	return strings.Split(annotVal, ","), nil
}

func getNamespaceLabelContext(ctx context.Context, k8s kubernetes.Interface,
	namespace, labelKey string) ([]string, error) {
	v1namespace, err := k8s.CoreV1().Namespaces().Get(ctx, namespace, v1.GetOptions{})
	if err != nil {
		return nil, fmt.Errorf("can not get a Namespace %s: %w", namespace, err)
	}

	labelVal, ok := v1namespace.Labels[labelKey]
	if !ok {
		return nil, fmt.Errorf("%w: no Namespace label %q value", ErrNoOwnerData, labelKey)
	}

	return strings.Split(labelVal, ","), nil
}

func getNamespaceAnnotationContext(ctx context.Context, k8s kubernetes.Interface,
	namespace, annotationKey string) ([]string, error) {
	v1namespace, err := k8s.CoreV1().Namespaces().Get(ctx, namespace, v1.GetOptions{})
	if err != nil {
		return nil, fmt.Errorf("can not get a Namespace %s: %w", namespace, err)
	}

	annotVal, ok := v1namespace.Annotations[annotationKey]
	if !ok {
		return nil, fmt.Errorf("%w: no Namespace annotation %q value", ErrNoOwnerData, annotationKey)
	}

	return strings.Split(annotVal, ","), nil
}
